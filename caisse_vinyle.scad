module 2D_to_3D_square(dimensions) {
	if (extrusion) {
		linear_extrude(height = dimensions.z)
			square([dimensions.x,
					dimensions.y]);
		
	} else {
		square([dimensions.x,
				dimensions.y]);
	}
}

module plateau_central(dimensions_plateau, epaisseur_cloison_largeur, epaisseur_cloison_longueur, largeur_dent_centrale_intercalaire, epaisseur_intercalaire, marge_prehension, espacement_intercalaire, nombre_dents_longueur, nombre_dents_largeur) {
	difference() {
	echo(- (epaisseur_cloison_longueur + kerf));
		translate([1 / 2 * kerf, 1 / 2 * kerf, 0])
			2D_to_3D_square([	dimensions_plateau.x + kerf,
								dimensions_plateau.y + kerf,
								dimensions_plateau.z]);
		translate([	marge_prehension,
					(dimensions_plateau.y - largeur_dent_centrale_intercalaire) / 2,
					0])
			empreintes_intercalaires(	largeur_dent_centrale_intercalaire,
										epaisseur_intercalaire,
										dimensions_plateau.z,
										espacement_intercalaire,
										dimensions_plateau.x,
										marge_prehension);
	}
    translate([1/2 * kerf, - 5.4, 0])
		empreintes_dents(		epaisseur_cloison_longueur,
								dimensions_plateau.z,
								dimensions_plateau.x,
								nombre_dents_longueur,
								false);
	translate([1/2 * kerf, dimensions_plateau.y + kerf, 0])
		empreintes_dents(	epaisseur_cloison_longueur,
							dimensions_plateau.z,
							dimensions_plateau.x,
							nombre_dents_longueur,
							false);
	translate([0, 0, 0]) rotate([0, 0, 90])
		empreintes_dents(	epaisseur_cloison_largeur,
							dimensions_plateau.z,
							dimensions_plateau.y,
							nombre_dents_largeur,
							false);
	translate([dimensions_plateau.x + 2 * epaisseur_cloison_largeur + kerf, 0, 0]) rotate([0, 0, 90])
			empreintes_dents(	epaisseur_cloison_largeur,
						dimensions_plateau.z,
							dimensions_plateau.y,
							nombre_dents_largeur,
							false);
}
module empreintes_dents(dimension_y, dimension_z, longueur_a_denter, nombre_dents, extrusion) {
	longueur_dent = longueur_a_denter / (2 * nombre_dents + 1);
    for (i = [0 : nombre_dents - 1]) {
		position = [ (1 + i * 2) * longueur_dent, 0, 0];
        if (extrusion) {
			translate(position)
				2D_to_3D_square([longueur_dent - kerf, dimension_y - kerf, dimension_z]);
		} else {
			translate(position)
				2D_to_3D_square([longueur_dent + kerf, dimension_y + kerf, dimension_z]);
		}
    }
}
module empreintes_intercalaires(largeur_dent_centrale, epaisseur_intercalaire, epaisseur_support, espacement_intercalaire, profondeur_support, marge_prehension) {
		profondeur_disponible = profondeur_support - marge_prehension * 2;
		taille_section = espacement_intercalaire + epaisseur_intercalaire;
		reste = profondeur_disponible % taille_section;
		profondeur_utile = profondeur_disponible - reste;
		nombre_sections = profondeur_utile / taille_section;
		for (i = [0 : nombre_sections - 1]) {
			translate([reste / 2 + espacement_intercalaire + taille_section * i, 0, 0])
				2D_to_3D_square([	epaisseur_intercalaire - kerf, 	
									largeur_dent_centrale_intercalaire - kerf, 
									epaisseur_support]);
		}
}

module cloison_longueur(dimensions_cloison_longueur, epaisseur_cloison_largeur, epaisseur_intercalaire, dimensions_plateau, hauteur_pieds, profondeur_dent_superieur_intercalaire, espacement_intercalaire, marge_prehension, nombre_dents_plateau, hauteur_utile_bords, nombre_dents_hauteur) {
    module extrusion_pieds(dimensions_cloison_longueur, hauteur) {
		longueur_pied = 20;
		longueur_extrusion = dimensions_cloison_longueur.x - longueur_pied * 2;
		translate([(dimensions_cloison_longueur.x - longueur_extrusion) / 2, dimensions_cloison_longueur.y - hauteur + kerf, 0])
			2D_to_3D_square([	longueur_extrusion - kerf,
								hauteur,
								dimensions_cloison_longueur.z]);
    }    
    
	difference() {
		2D_to_3D_square([	dimensions_cloison_longueur.x + kerf,
							dimensions_cloison_longueur.y + kerf,
							dimensions_cloison_longueur.z]);
		
		extrusion_pieds(dimensions_cloison_longueur, hauteur_pieds);
		translate([2 * epaisseur_cloison_largeur, dimensions_cloison_longueur.y - (2 * dimensions_plateau.z + hauteur_pieds), 0])
			empreintes_dents(	dimensions_plateau.z,
								dimensions_cloison_longueur.z,
								dimensions_plateau.x,
								nombre_dents_plateau);
		translate([2* epaisseur_cloison_largeur, 0, 0]) rotate([0, 0, 90])
			empreintes_dents(	epaisseur_cloison_largeur,
								dimensions_cloison_longueur.z,
								hauteur_utile_bords,
								nombre_dents_hauteur,
								true);
		
		translate([dimensions_plateau.x + 3 * epaisseur_cloison_largeur, 0, 0]) rotate([0, 0, 90])
			empreintes_dents(	epaisseur_cloison_largeur,
								dimensions_cloison_longueur.z,
								hauteur_utile_bords,
								nombre_dents_hauteur,
								true);
		translate([2 * epaisseur_cloison_largeur + marge_prehension, 0, 0])
			empreintes_intercalaires(	profondeur_dent_superieur_intercalaire,
										epaisseur_intercalaire,
										dimensions_cloison_longueur.z,
										espacement_intercalaire,
										dimensions_cloison_longueur.x - 2 * 2 * epaisseur_cloison_largeur,
										marge_prehension);
    }
    
}

module cloison_largeur(dimensions_cloison_largeur, epaisseur_plateau, epaisseur_cloison_longueur, largeur_utile_plateau, hauteur_utile_bords, nombre_dents_hauteur, nombre_dents_longueur) {
	difference() {
		2D_to_3D_square([	dimensions_cloison_largeur.x + kerf,
							dimensions_cloison_largeur.y + kerf,	
							dimensions_cloison_largeur.z]);
		translate([dimensions_cloison_largeur.x - epaisseur_plateau, -epaisseur_cloison_longueur, 0]) rotate([0, 0, 90])
		empreintes_dents(	epaisseur_plateau,
							dimensions_cloison_largeur.z,
							largeur_utile_plateau,
							nombre_dents_longueur,
							true);
    }
    translate([0, -epaisseur_cloison_longueur - kerf, 0])
		empreintes_dents(	epaisseur_cloison_longueur,
							dimensions_cloison_largeur.z,
							hauteur_utile_bords,
							nombre_dents_hauteur,
							false);
	translate([0, dimensions_cloison_largeur.y, 0])
		empreintes_dents(	epaisseur_cloison_longueur,
							dimensions_cloison_largeur.z,
							hauteur_utile_bords,
							nombre_dents_hauteur,
							false);
	
}
module intercalaire(dimensions_intercalaire, largeur_dent_centrale, profondeur_dent_superieur, epaisseur_cloison_longueur, epaisseur_plateau) {
	decroche_dent_superieur = 10;
	difference() {
		2D_to_3D_square([	profondeur_dent_superieur + decroche_dent_superieur + kerf, 
							dimensions_intercalaire.y + kerf, 
							dimensions_intercalaire.z]);
		translate([profondeur_dent_superieur, epaisseur_cloison_longueur, 0]) 
			2D_to_3D_square([	decroche_dent_superieur + kerf,
								dimensions_intercalaire.y - 2 * epaisseur_cloison_longueur - kerf,
								dimensions_intercalaire.z]);
	}
	translate([profondeur_dent_superieur, epaisseur_cloison_longueur * 2, 0]) 
		2D_to_3D_square([	decroche_dent_superieur + kerf,
							dimensions_intercalaire.y - 2 * 2 * epaisseur_cloison_longueur + kerf,
							dimensions_intercalaire.z]);
	
	translate([0, (dimensions_intercalaire.y - largeur_dent_centrale) / 2, 0]) 
		2D_to_3D_square([	dimensions_intercalaire.x + kerf, 
							largeur_dent_centrale + kerf,
							dimensions_intercalaire.z]);
	dimensions_polygones = [	[profondeur_dent_superieur + decroche_dent_superieur + kerf, 2 * epaisseur_cloison_longueur ],
								[profondeur_dent_superieur + decroche_dent_superieur, (dimensions_intercalaire.y - largeur_dent_centrale) / 2],
								[dimensions_intercalaire.x - epaisseur_plateau + kerf, (dimensions_intercalaire.y - largeur_dent_centrale) / 2]];
	//linear_extrude(height = dimensions_intercalaire.z)
		polygon(dimensions_polygones);
	translate([0, dimensions_intercalaire.y, 0])
		//linear_extrude(height = dimensions_intercalaire.z)
			rotate([180, 0, 0])
			polygon(dimensions_polygones);
}

///////// Parametres /////////
extrusion = false;
kerf = 0.4;
hauteur_pieds = 10;
hauteur_bords = 150;
nombre_dents_hauteur = 2;
marge_prehension = 30;
espacement_intercalaire = 30;
dimensions_plateau = [500, 320, 10];
nombre_dents_longueur_plateau = 5;
nombre_dents_largeur_plateau = 5;
epaisseur_cloison_longueur = 5;
dimensions_cloison_largeur = [hauteur_bords + 2 * dimensions_plateau.z, dimensions_plateau.y, 5];
dimensions_cloison_longueur = [dimensions_plateau.x + dimensions_cloison_largeur.z * 2 * 2, hauteur_bords + 2 * dimensions_plateau.z + hauteur_pieds, epaisseur_cloison_longueur];
profondeur_dent_superieur_intercalaire = 24;
largeur_dent_centrale_intercalaire = 30;
dimensions_intercalaire = [hauteur_bords + dimensions_plateau.z, dimensions_plateau.y + 2 * 2 * dimensions_cloison_longueur.z, 5];
//////////////////////////////
echo([dimensions_cloison_largeur.z, dimensions_cloison_longueur.z, 0]);
translate([dimensions_cloison_largeur.z, dimensions_cloison_longueur.z, 0])
plateau_central(dimensions_plateau, dimensions_cloison_largeur.z, dimensions_cloison_longueur.z, largeur_dent_centrale_intercalaire, dimensions_intercalaire.z, marge_prehension, espacement_intercalaire, nombre_dents_longueur_plateau,
	nombre_dents_largeur_plateau);
//translate([0, 0, -30]) rotate([-90, 0, 0]) //////
translate([	-dimensions_cloison_largeur.z,
			-dimensions_cloison_longueur.y - 10,
			0])
	cloison_longueur(	dimensions_cloison_longueur,
						dimensions_cloison_largeur.z,
						dimensions_intercalaire.z,
						dimensions_plateau,
						hauteur_pieds,
						profondeur_dent_superieur_intercalaire,
						espacement_intercalaire,
						marge_prehension,
						nombre_dents_longueur_plateau,
						hauteur_bords,
						nombre_dents_hauteur);

/*translate([	-dimensions_cloison_largeur.z,
			dimensions_plateau.y + 2 * dimensions_cloison_longueur.z +dimensions_cloison_longueur.y + 10,
			dimensions_cloison_longueur.z])
			rotate([180, 0, 0])
//translate(-[0, -dimensions_cloison_longueur.y - dimensions_cloison_longueur.z * 3, 155]) rotate([90, 0, 0]) //////
	cloison_longueur(	dimensions_cloison_longueur,
						dimensions_cloison_largeur.z,
						dimensions_intercalaire.z,
						dimensions_plateau,
						hauteur_pieds,
						profondeur_dent_superieur_intercalaire,
						espacement_intercalaire,
						marge_prehension,
						nombre_dents_longueur_plateau,
						hauteur_bords,
						nombre_dents_hauteur);*/
//translate([0, 0, -20]) rotate([0, 90, 0]) //////
translate([	-dimensions_cloison_largeur.x - 10,
			dimensions_cloison_longueur.z,
			0]) 
	cloison_largeur(	dimensions_cloison_largeur,
						dimensions_plateau.z,
						dimensions_cloison_longueur.z,
						dimensions_plateau.y,
						hauteur_bords,
						nombre_dents_hauteur,
						nombre_dents_largeur_plateau);
/*translate([	dimensions_plateau.x + 2 * dimensions_cloison_largeur.z + dimensions_cloison_largeur.x + 10,
			dimensions_cloison_longueur.z,
			dimensions_cloison_largeur.z]) rotate([0, 180, 0])
//translate(-[-185, 0, 155]) rotate([0, -90, 0]) //////
	cloison_largeur(	dimensions_cloison_largeur,
						dimensions_plateau.z,
						dimensions_cloison_longueur.z,
						dimensions_plateau.y,
						hauteur_bords,
						nombre_dents_hauteur,
						nombre_dents_largeur_plateau);*/
//translate([dimensions_cloison_largeur.z + marge_prehension + 40, 0, -190])rotate([0, 90, 0]) //////
translate([	-dimensions_cloison_largeur.x - 10,
			-dimensions_cloison_longueur.z, 
			0] - [dimensions_intercalaire.x + 10, 0, 0]) 
	intercalaire(	dimensions_intercalaire,
					largeur_dent_centrale_intercalaire, 
					profondeur_dent_superieur_intercalaire,
					dimensions_cloison_longueur.z,
					dimensions_plateau.z);